#include <iostream>
#include <ctime>

using namespace std;

class BST
{
public:
	BST();
	BST(int value);
	~BST();

	void add(int value);
	int find(int value);
	void printPreorder();
	void printInorder();
	void printPostorder();
	void remove(int value);

protected:
	class Node
	{
	public:
		Node(int value);

		int getValue();
		Node* getLeft();
		Node* getRight();

		void setValue(int value);
		void setLeft(Node* left);
		void setRight(Node* right);

	private:
		int value;
		Node* left;
		Node* right;
	};

	Node* root;

	void add(int value, Node* node);
	Node* find(int value, Node* node);
	void preorder(Node* node);
	void inorder(Node* node);
	void postorder(Node* node);
	Node* remove(int value, Node* node);
	Node* findSuccessor(Node* node);
	Node* removeSuccessor(Node* node);
	void free(Node* node);
};

BST::Node::Node(int value) : value(value)
{
	left = nullptr;
	right = nullptr;
}

int BST::Node::getValue()
{
	return value;
}

BST::Node* BST::Node::getLeft()
{
	return left;
}

BST::Node* BST::Node::getRight()
{
	return right;
}

void BST::Node::setValue(int value)
{
	this->value = value;
}

void BST::Node::setLeft(Node* left)
{
	this->left = left;
}

void BST::Node::setRight(Node* right)
{
	this->right = right;
}

BST::BST()
{
	root = nullptr;
}

BST::BST(int value)
{
	root = new Node(value);
}

BST::~BST()
{
	free(root);
}

void BST::add(int value)
{
	add(value, root);
}

int BST::find(int value)
{
	Node* node = find(value, root);
	return (node == nullptr) ? NULL : node->getValue();
}

void BST::printPreorder()
{
	cout << "Preorder:" << endl;
	preorder(root);
	cout << endl;
}

void BST::printInorder()
{
	cout << "Inorder:" << endl;
	inorder(root);
	cout << endl;
}

void BST::printPostorder()
{
	cout << "Postorder:" << endl;
	postorder(root);
	cout << endl;
}

void BST::remove(int value)
{
	root = remove(value, root);
}

void BST::add(int value, Node* node)
{
	if (root == nullptr)
		root = new Node(value);
	else
	{
		if (node->getValue() == value)
			cout << "Element o wartosci " << value << " juz istnieje!\n";
		else if (node->getValue() > value)
		{
			if (node->getLeft() == nullptr)
				node->setLeft(new Node(value));
			else
				add(value, node->getLeft());
		}
		else
		{
			if (node->getRight() == nullptr)
				node->setRight(new Node(value));
			else
				add(value, node->getRight());
		}
	}
}

BST::Node* BST::find(int value, Node* node)
{
	if (node == nullptr)
		cout << "Element o wartosci " << value << " nie istnieje!\n";
	else
	{
		if (node->getValue() == value)
			cout << "Znaleziono: " << node->getValue() << endl;
		else if (node->getValue() > value)
			node = find(value, node->getLeft());
		else
			node = find(value, node->getRight());
	}

	return node;
}

void BST::preorder(Node* node)
{
	if (node != nullptr)
	{
		cout << node->getValue() << ", ";
		preorder(node->getLeft());
		preorder(node->getRight());
	}
}

void BST::inorder(Node* node)
{
	if (node != nullptr)
	{
		inorder(node->getLeft());
		cout << node->getValue() << ", ";
		inorder(node->getRight());
	}
}

void BST::postorder(Node* node)
{
	if (node != nullptr)
	{
		postorder(node->getLeft());
		postorder(node->getRight());
		cout << node->getValue() << ", ";
	}
}

BST::Node* BST::remove(int value, Node* node)
{
	if (node == nullptr)
	{
		cout << "Element o wartosci " << value << " nie istnieje!\n";
		return node;
	}
	else if (node->getValue() > value)
		node->setLeft(remove(value, node->getLeft()));
	else if (node->getValue() < value)
		node->setRight(remove(value, node->getRight()));
	else
	{
		if (node->getLeft() == nullptr && node->getRight() == nullptr)
		{
			delete node;
			node = nullptr;
		}
		else if (node->getLeft() == nullptr)
		{
			Node* toRemove = node;
			node = node->getRight();
			delete toRemove;
		}
		else if (node->getRight() == nullptr)
		{
			Node* toRemove = node;
			node = node->getLeft();
			delete toRemove;
		}
		else
		{
			Node* toRemove = node;
			node = findSuccessor(toRemove->getRight());
			node->setRight(removeSuccessor(toRemove->getRight()));
			node->setLeft(toRemove->getLeft());
			delete toRemove;
		}
		cout << "Usunieto " << value << endl;
	}

	return node;
}

BST::Node* BST::findSuccessor(Node* node)
{
	if (node->getLeft() == nullptr)
		return node;

	return findSuccessor(node->getLeft());
}

BST::Node* BST::removeSuccessor(Node* node)
{
	if (node->getLeft() == nullptr)
		return node->getRight();

	node->setLeft(removeSuccessor(node->getLeft()));

	return node;
}

void BST::free(Node* node)
{
	if (node != nullptr)
	{
		free(node->getLeft());
		free(node->getRight());
		delete node;
	}
}

int main()
{
	BST* bst = new BST(100);
	bst->add(50);
	bst->add(120);
	bst->add(40);
	bst->add(45);
	bst->add(150);
	bst->add(35);
	bst->add(48);

	bst->printPreorder();

	bst->remove(120);
	bst->remove(40);
	bst->remove(50);

	bst->printPreorder();

	bst->find(48);
	bst->find(70);

	srand((unsigned int)time(NULL));
	for (int i = 0; i < 10; i++)
		bst->add(rand() % 300);

	bst->printPreorder();
	bst->printInorder();
	bst->printPostorder();

	delete bst;

	system("pause");
	return 0;
}
